CREATE TABLE IF NOT EXISTS words (
    "id" UUID PRIMARY KEY,
    "uz_word" VARCHAR,
    "en_word" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
