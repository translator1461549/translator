package handlers

import (
	"translator/api/http"
	ts "translator/genproto/translator_service"

	"github.com/gin-gonic/gin"
)

// CreateWord godoc
// @ID create_word
// @Router /word [POST]
// @Summary Create Word
// @Description Create Word
// @Tags Word
// @Accept json
// @Produce json
// @Param word body translator_service.CreateWord true "CreateWordRequestBody"
// @Success 201 {object} http.Response{data=translator_service.Word} "Word data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateWord(c *gin.Context) {
	var word ts.CreateWord

	err := c.ShouldBindJSON(&word)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.TranslateService().Create(
		c.Request.Context(),
		&word,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetWordList godoc
// @ID get_word_list
// @Router /words [GET]
// @Summary Get Word List
// @Description  Get Word List
// @Tags Word
// @Accept json
// @Produce json
// @Param page query integer false "page"
// @Param limit query integer false "limit"
// @Success 200 {object} http.Response{data=translator_service.WordList} "WordList"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetWordList(c *gin.Context) {
	page, err := h.getPageParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.TranslateService().GetList(
		c.Request.Context(),
		&ts.GetWordListReq{
			Limit: int64(limit),
			Page:  int64(page),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateWord godoc
// @ID update_Word
// @Router /word [PUT]
// @Summary Update Wird
// @Description Update Wird
// @Tags Word
// @Accept json
// @Produce json
// @Param word body translator_service.Word true "WordBody"
// @Success 200 {object} http.Response{data=translator_service.Word} "Word data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateWord(c *gin.Context) {
	var word ts.Word

	err := c.ShouldBindJSON(&word)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	_, err = h.services.TranslateService().Update(
		c.Request.Context(),
		&word,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, gin.H{
		"success": true,
	})
}

// Search godoc
// @ID word_search
// @Router /translate [GET]
// @Summary Get Word Translate
// @Description  Get Word Translate
// @Tags Word
// @Accept json
// @Produce json
// @Param language query string false "language"
// @Param word query string false "word"
// @Success 200 {object} http.Response{data=translator_service.Word} "WordList"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Translate(c *gin.Context) {

	resp, err := h.services.TranslateService().Translate(
		c.Request.Context(),
		&ts.TranslateWord{
			Language: c.Query("language"),
			Word:     c.Query("word"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
