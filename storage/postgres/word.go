package postgres

import (
	"context"
	"fmt"
	pb "translator/genproto/translator_service"
	"translator/pkg/helper"
	"translator/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type wordRepo struct {
	db *pgxpool.Pool
}

func NewWordRepo(db *pgxpool.Pool) storage.WordRepoI {
	return &wordRepo{
		db: db,
	}
}

func (r *wordRepo) Create(ctx context.Context, entity *pb.CreateWord) (*pb.Word, error) {
	var resp pb.Word
	query := `
	INSERT INTO words (
		id,
		uz_word,
		en_word
	) VALUES (
		$1,
		$2,
		$3
	) RETURNING 
		id,
		uz_word,
		en_word
	`

	uuid, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	err = r.db.QueryRow(ctx, query,
		uuid.String(),
		entity.UzWord,
		entity.EnWord,
	).Scan(
		&resp.Id,
		&resp.UzWord,
		&resp.EnWord,
	)

	return &resp, err
}

func (r *wordRepo) GetList(ctx context.Context, queryParam *pb.GetWordListReq) (*pb.WordList, error) {
	res := &pb.WordList{}
	params := make(map[string]interface{})
	var arr []interface{}
	query := `
	SELECT
		COUNT(id) OVER(),
		id,
		uz_word,
		en_word
	FROM
		words
	ORDER BY created_at DESC `

	offset := " OFFSET 0"
	limit := " LIMIT 10"

	if queryParam.Page > 0 {
		params["offset"] = (queryParam.Page - 1) * queryParam.Limit
		offset = " OFFSET :offset"
	}

	if queryParam.Limit > 0 {
		params["limit"] = queryParam.Limit
		limit = " LIMIT :limit"
	}

	query = query + offset + limit

	query, arr = helper.ReplaceQueryParams(query, params)
	rows, err := r.db.Query(ctx, query, arr...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &pb.Word{}

		err = rows.Scan(
			&res.Count,
			&obj.Id,
			&obj.UzWord,
			&obj.EnWord,
		)

		if err != nil {
			return res, err
		}

		res.Words = append(res.Words, obj)
	}

	return res, nil
}

func (r *wordRepo) Update(ctx context.Context, entity *pb.Word) (*pb.Empty, error) {
	query := `
	UPDATE words SET
		en_word = :en_word,
		uz_word = :uz_word
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":      entity.Id,
		"en_word": entity.EnWord,
		"uz_word": entity.UzWord,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	_, err := r.db.Exec(ctx, q, arr...)
	if err != nil {
		return nil, err
	}

	return &pb.Empty{}, err
}

func (r *wordRepo) Translate(ctx context.Context, req *pb.TranslateWord) (res *pb.WordList, err error) {
	res = &pb.WordList{}

	query := `
	SELECT
		COUNT(id) OVER(),
		id,
		uz_word,
		en_word
	FROM
		words
	WHERE true `

	if req.Language == "uz" {
		query += ` AND uz_word LIKE '%` + req.Word + `%' `
	} else if req.Language == "en" {
		query += ` AND en_word LIKE '%` + req.Word + `%' `
	}

	query += ` ORDER BY created_at DESC `

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &pb.Word{}

		err = rows.Scan(
			&res.Count,
			&obj.Id,
			&obj.UzWord,
			&obj.EnWord,
		)

		if err != nil {
			fmt.Println("in here: ", err)
			return res, err
		}

		res.Words = append(res.Words, obj)
	}
	return res, nil
}
