package storage

import (
	"context"
	"errors"
	pb "translator/genproto/translator_service"
)

var ErrorTheSameId = errors.New("cannot use the same uuid for 'id' and 'parent_id' fields")
var ErrorProjectId = errors.New("not valid 'project_id'")

type StorageI interface {
	CloseDB()
	Word() WordRepoI
}

type WordRepoI interface {
	Create(ctx context.Context, createReq *pb.CreateWord) (pKey *pb.Word, err error)
	GetList(ctx context.Context, queryParam *pb.GetWordListReq) (res *pb.WordList, err error)
	Update(ctx context.Context, updateReq *pb.Word) (empty *pb.Empty, err error)
	Translate(ctx context.Context, req *pb.TranslateWord) (res *pb.WordList, err error)
}
