package service

import (
	"context"
	ts "translator/genproto/translator_service"
	"translator/grpc/client"

	"translator/config"
	"translator/storage"

	"translator/pkg/logger"
)

type wordService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	ts.UnimplementedTranslatorServiceServer
}

func NewWordService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, svcs client.ServiceManagerI) *wordService {
	return &wordService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: svcs,
	}
}

func (s *wordService) Create(ctx context.Context, req *ts.CreateWord) (*ts.Word, error) {
	return s.strg.Word().Create(ctx, req)
}

func (s *wordService) GetList(ctx context.Context, req *ts.GetWordListReq) (*ts.WordList, error) {
	return s.strg.Word().GetList(ctx, req)
}

func (s *wordService) Update(ctx context.Context, req *ts.Word) (*ts.Empty, error) {
	return s.strg.Word().Update(ctx, req)
}

func (s *wordService) Translate(ctx context.Context, req *ts.TranslateWord) (*ts.WordList, error) {
	return s.strg.Word().Translate(ctx, req)
}
