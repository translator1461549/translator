package grpc

import (
	"translator/config"
	"translator/genproto/translator_service"
	"translator/grpc/client"
	"translator/grpc/service"
	"translator/storage"

	"translator/pkg/logger"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, svcs client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer(
		grpc.UnaryInterceptor(logger.LogUnaryInterceptor(log)),
	)

	translator_service.RegisterTranslatorServiceServer(grpcServer, service.NewWordService(cfg, log, strg, svcs))

	reflection.Register(grpcServer)
	return
}
