package client

import (
	"translator/config"
	ts "translator/genproto/translator_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	TranslateService() ts.TranslatorServiceClient
}

type grpcClients struct {
	translateService ts.TranslatorServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connTranslateService, err := grpc.Dial(
		cfg.WordServiceHost+cfg.WordGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		translateService: ts.NewTranslatorServiceClient(connTranslateService),
	}, nil
}

func (g *grpcClients) TranslateService() ts.TranslatorServiceClient {
	return g.translateService
}
