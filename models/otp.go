package models

import "time"

type GenerateOtpRequest struct {
	UserID       string        `json:"user_id"`
	ExpiresAfter time.Duration `json:"expires_after"`
}

type Otp struct {
	ID        string
	UserID    string
	Code      string
	ExpiresAt time.Time
	CreatedAt time.Time
}

type VerifyOtpRequest struct {
	ID   string `json:"id"`
	Code string `json:"code"`
}
